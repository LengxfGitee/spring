package org.springframework.lengxf.context.service;

import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Service;

@Service
public class UserService {

	private OrderService orderService;

	public UserService(OrderService orderService) {
		this.orderService = orderService;
	}


	public String getUser() {
		if (orderService == null) {
			System.out.println("这个被改变为null");
		}
		return "user";
	}

	@Service
	public class UserSe1 {
		private String s;


		@Bean
		public String getS() {
			return s;
		}

	}
}
