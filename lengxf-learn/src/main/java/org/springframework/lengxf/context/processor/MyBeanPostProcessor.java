package org.springframework.lengxf.context.processor;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.BeanPostProcessor;
import org.springframework.lengxf.context.service.OrderService;
import org.springframework.stereotype.Component;

@Component
public class MyBeanPostProcessor implements BeanPostProcessor {
	@Override
	public Object postProcessBeforeInitialization(Object bean, String beanName) throws BeansException {
		if(beanName.equals("orderService")){
			((OrderService)bean).setName("lengxf");
			System.out.println("MyBeanPostProcessor # postProcessBeforeInitialization");
		}
		return bean;
	}

	@Override
	public Object postProcessAfterInitialization(Object bean, String beanName) throws BeansException {
		if(beanName.equals("orderService")){
			System.out.println("MyBeanPostProcessor # postProcessAfterInitialization");
		}
		return bean;
	}
}
