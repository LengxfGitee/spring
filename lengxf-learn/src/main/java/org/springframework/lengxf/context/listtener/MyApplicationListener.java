package org.springframework.lengxf.context.listtener;

import org.springframework.context.ApplicationEvent;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextStoppedEvent;
import org.springframework.stereotype.Component;

@Component
public class MyApplicationListener implements ApplicationListener<ContextStoppedEvent> {



/**
 *
 * @author Lengxf
 **/
	@Override
	public void onApplicationEvent(ContextStoppedEvent event) {
		System.out.println("MyApplicationListener # onApplicationEvent ");
	}
}
