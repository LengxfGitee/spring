package org.springframework.lengxf.context.processor;

import org.springframework.beans.factory.InitializingBean;
import org.springframework.stereotype.Component;

@Component
public class MyInitializingBean implements InitializingBean {

	private String name;

	public String getName() {
		return name;
	}

	@Override
	public void afterPropertiesSet() throws Exception {
		name = "lengxf";
	}


}
