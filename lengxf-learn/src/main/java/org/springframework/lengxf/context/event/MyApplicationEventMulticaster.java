package org.springframework.lengxf.context.event;

import org.springframework.context.ApplicationEvent;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ApplicationEventMulticaster;
import org.springframework.core.ResolvableType;
import org.springframework.stereotype.Component;

import java.util.function.Predicate;

@Component
public class MyApplicationEventMulticaster implements ApplicationEventMulticaster {
	@Override
	public void addApplicationListener(ApplicationListener<?> listener) {
		System.out.println("MyApplicationEventMulticaster # addApplicationListener");
	}

	@Override
	public void addApplicationListenerBean(String listenerBeanName) {
		System.out.println("MyApplicationEventMulticaster # addApplicationListenerBean");
	}

	@Override
	public void removeApplicationListener(ApplicationListener<?> listener) {
		System.out.println("MyApplicationEventMulticaster # removeApplicationListener");
	}

	@Override
	public void removeApplicationListenerBean(String listenerBeanName) {
		System.out.println("MyApplicationEventMulticaster # removeApplicationListenerBean");
	}

	@Override
	public void removeApplicationListeners(Predicate<ApplicationListener<?>> predicate) {
		System.out.println("MyApplicationEventMulticaster # removeApplicationListeners");
	}

	@Override
	public void removeApplicationListenerBeans(Predicate<String> predicate) {
		System.out.println("MyApplicationEventMulticaster # removeApplicationListenerBeans");
	}

	@Override
	public void removeAllListeners() {
		System.out.println("MyApplicationEventMulticaster # removeAllListeners");
	}

	@Override
	public void multicastEvent(ApplicationEvent event) {
		System.out.println("MyApplicationEventMulticaster # multicastEvent");
	}

	@Override
	public void multicastEvent(ApplicationEvent event, ResolvableType eventType) {
		System.out.println("MyApplicationEventMulticaster # multicastEvent");
	}
}
