package org.springframework.lengxf.context.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Service;

@Order(1)
@Service
public class OrderService {


	private String name;

	public void setName(String name) {
		this.name = name;
	}

	@Autowired
	@Qualifier("userService")
	private UserService userService;


	public String test1() {
		System.out.println("------------------ " + name);
		userService.getUser();
		return "OrderService1";
	}

	@Bean
	public String test2() {
		return "Order#test2";
	}

}
