package org.springframework.lengxf.context.processor;

import org.springframework.beans.BeansException;
import org.springframework.beans.MutablePropertyValues;
import org.springframework.beans.PropertyValues;
import org.springframework.beans.factory.config.InstantiationAwareBeanPostProcessor;
import org.springframework.lengxf.context.service.UserService;

//@Component
public class MyInstantiationAwareBeanPostProcessor implements InstantiationAwareBeanPostProcessor {
	@Override
	public PropertyValues postProcessProperties(PropertyValues pvs, Object bean, String beanName) throws BeansException {
		if (beanName.equals("orderService")) {
			System.out.println("postProcessProperties_postProcessProperties..");
			//可以修改bean的属性
			MutablePropertyValues p = new MutablePropertyValues();
			p.add("userService", new UserService(null));
			//比如说在这里整点骚操作 服务就不能正常起来了
			return p;
		}
		return pvs;
	}
}
