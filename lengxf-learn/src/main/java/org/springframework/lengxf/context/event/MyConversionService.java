package org.springframework.lengxf.context.event;

import org.springframework.core.convert.ConversionService;
import org.springframework.core.convert.TypeDescriptor;
import org.springframework.stereotype.Component;

@Component("conversionService")
public class MyConversionService implements ConversionService {
	@Override
	public boolean canConvert(Class<?> sourceType, Class<?> targetType) {
		System.out.println("MyConversionService # canConvert");
		return false;
	}

	@Override
	public boolean canConvert(TypeDescriptor sourceType, TypeDescriptor targetType) {
		System.out.println("MyConversionService # canConvert");
		return false;
	}

	@Override
	public <T> T convert(Object source, Class<T> targetType) {
		System.out.println("MyConversionService # convert");
		return null;
	}

	@Override
	public Object convert(Object source, TypeDescriptor sourceType, TypeDescriptor targetType) {
		System.out.println("MyConversionService # convert");
		return null;
	}
}
