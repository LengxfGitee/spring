package org.springframework.lengxf.context;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.lengxf.context.service.OrderService;

@ComponentScan("org.springframework.lengxf.context")
public class CtxApplication {

	public static void main(String[] args) {

		ApplicationContext ctx = new AnnotationConfigApplicationContext(CtxApplication.class);
		OrderService orderService = (OrderService)ctx.getBean("orderService");
		System.out.println(orderService.test1());

	}

}