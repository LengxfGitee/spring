package org.springframework.lengxf.aop.service;

import org.springframework.lengxf.aop.aspect.AopTest;
import org.springframework.stereotype.Service;

@Service
public class AopService {

	@AopTest
	public String test1() {
		System.out.println(" AopService # test1");
		return "success";
	}

}
