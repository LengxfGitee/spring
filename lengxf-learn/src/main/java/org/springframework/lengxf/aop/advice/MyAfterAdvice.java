package org.springframework.lengxf.aop.advice;

import org.springframework.aop.AfterReturningAdvice;
import org.springframework.stereotype.Component;

import java.lang.reflect.Method;

//@Component
public class MyAfterAdvice implements AfterReturningAdvice {


	@Override
	public void afterReturning(Object returnValue, Method method, Object[] args, Object target) throws Throwable {
		System.out.println("方法执行后");
	}
}
