package org.springframework.lengxf.aop.aspect;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.springframework.stereotype.Component;

@Aspect
@Component
public class LengxfAspect {


	@Before(value = "@annotation(aopTest)")
	public void before(AopTest aopTest) {
		System.out.println(" 注解切片前...");
	}

	@After(value = "@annotation(aopTest)")
	public void after(AopTest aopTest) {
		System.out.println(" 注解切片后...");
	}

	@Around(value = "@annotation(aopTest)")
	public Object around(ProceedingJoinPoint pointcut, AopTest aopTest) throws Throwable {
		System.out.println(" 注解环绕切片前...");
		Object proceed = pointcut.proceed();
		System.out.println(" 注解环绕切片后...");
		return proceed;
	}
}
