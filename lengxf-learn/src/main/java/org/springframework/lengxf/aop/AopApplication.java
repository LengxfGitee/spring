package org.springframework.lengxf.aop;

import org.springframework.aop.support.NameMatchMethodPointcutAdvisor;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.EnableAspectJAutoProxy;
import org.springframework.lengxf.aop.advice.MyBeforeAdvice;
import org.springframework.lengxf.aop.service.AopService;

@EnableAspectJAutoProxy
@ComponentScan("org.springframework.lengxf.aop")
public class AopApplication {

	//@Bean
	//public NameMatchMethodPointcutAdvisor advisor(){
	//	NameMatchMethodPointcutAdvisor nameMatchMethodPointcutAdvisor = new NameMatchMethodPointcutAdvisor();
	//	nameMatchMethodPointcutAdvisor.addMethodName("test1");
	//	nameMatchMethodPointcutAdvisor.setAdvice(new MyBeforeAdvice());
	//	return nameMatchMethodPointcutAdvisor;
	//}

	public static void main(String[] args) {
		ApplicationContext ctx = new AnnotationConfigApplicationContext(AopApplication.class);
		AopService aopService = ctx.getBean(AopService.class);
		aopService.test1();

		//ProxyFactory proxyFactory = new ProxyFactory();
		//proxyFactory.setTarget(new AopService());
		//proxyFactory.addAdvice(new MyBeforeAdvice());
		//proxyFactory.addAdvice(new MyAfterAdvice());
		//proxyFactory.addAdvice(new MyAroundAdvice());
		//AopService proxy = (AopService) proxyFactory.getProxy();
		//proxy.test1();
	}


}