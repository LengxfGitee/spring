package org.springframework.lengxf.aop.advice;

import org.aopalliance.intercept.MethodInterceptor;
import org.aopalliance.intercept.MethodInvocation;
import org.springframework.aop.AfterReturningAdvice;
import org.springframework.stereotype.Component;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.lang.reflect.Method;

//@Component
public class MyAroundAdvice implements MethodInterceptor {


	@Nullable
	@Override
	public Object invoke(@Nonnull MethodInvocation invocation) throws Throwable {
		System.out.println("执行around前..");
		Object proceed = invocation.proceed();
		System.out.println("执行around后..");
		return proceed;
	}
}
