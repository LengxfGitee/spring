package org.springframework.lengxf.tx.service;

import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class TxServiceA {
	private final TxServiceB txServiceB;
	private final JdbcTemplate jdbcTemplate;
	private final TxServiceC txServiceC;

	public TxServiceA(TxServiceB txServiceB, TxServiceC txServiceC, JdbcTemplate jdbcTemplate) {
		this.txServiceB = txServiceB;
		this.txServiceC = txServiceC;
		this.jdbcTemplate = jdbcTemplate;
	}

	static String sql = "INSERT INTO `test_1`.`test_a` ( `name`, `age`) VALUES ( 'TXA', 11);";


	@Transactional
	public String test() {

		//当  test() 方法上不存在 @Transactional 注解的时候 方法的事务不会生效
		insert();

		//txServiceC.test();
		txServiceC.testWithOutTx();

		txServiceB.test();
		txServiceB.testWithOutTx();

		System.out.println("org.springframework.lengxf.tx.service.TxServiceA.test");
		//throw new RuntimeException("模拟抛出异常....");
		return "success";
	}


	@Transactional
	public void insert() {
		jdbcTemplate.execute(sql);
	}


}
