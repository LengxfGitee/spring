package org.springframework.lengxf.tx.service;

import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class TxServiceB {

	private final JdbcTemplate jdbcTemplate;

	static String sql = "INSERT INTO `test_1`.`test_a` ( `name`, `age`) VALUES ( 'TXB', 2);";

	public TxServiceB(JdbcTemplate jdbcTemplate) {
		this.jdbcTemplate = jdbcTemplate;
	}

	@Transactional
	public String test() {
		jdbcTemplate.execute(sql);
		System.out.println("org.springframework.lengxf.tx.service.TxServiceB.test");
		throw new RuntimeException("主动抛出异常...>");
		//return "success";
	}

	public String testWithOutTx() {
		jdbcTemplate.execute(sql);
		System.out.println("org.springframework.lengxf.tx.service.TxServiceB.test");
		throw new RuntimeException("主动抛出异常...>");
		//return "success";
	}


}
