package org.springframework.lengxf.tx.service;

import org.springframework.stereotype.Service;

@Service
public class TxServiceD {

	private final TxServiceA txServiceA;

	public TxServiceD(TxServiceA txServiceA) {
		this.txServiceA = txServiceA;
	}

	public String test() {
		txServiceA.test();
		System.out.println("org.springframework.lengxf.tx.service.TxServiceD.test");
		//throw new RuntimeException("模拟抛出异常....");
		return "success";
	}


}
