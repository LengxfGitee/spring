package org.springframework.lengxf.tx;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.lengxf.tx.service.TxServiceA;
import org.springframework.lengxf.tx.service.TxServiceD;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@EnableTransactionManagement
@ComponentScan("org.springframework.lengxf.tx")
public class TxApplication {

	public static void main(String[] args) {
		// 事务生效比虚经过完成的bean的声明周期代理
		ApplicationContext ctx = new AnnotationConfigApplicationContext(TxApplication.class);
		TxServiceA serviceA = ctx.getBean(TxServiceA.class);
		System.out.println(serviceA.test());
	}


}