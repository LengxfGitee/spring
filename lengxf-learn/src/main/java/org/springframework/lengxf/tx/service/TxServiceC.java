package org.springframework.lengxf.tx.service;

import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class TxServiceC {

	private final JdbcTemplate jdbcTemplate ;
	static String sql = "INSERT INTO `test_1`.`test_a` ( `name`, `age`) VALUES ( 'TXC', 1);";

	public TxServiceC(JdbcTemplate jdbcTemplate) {
		this.jdbcTemplate = jdbcTemplate;
	}

	@Transactional
	public String test() {
		jdbcTemplate.execute(sql);
		System.out.println("org.springframework.lengxf.tx.service.TxServiceC.test");
		throw new RuntimeException("模拟抛出异常....");
		//return "success";
	}

	public void testWithOutTx() {
		jdbcTemplate.execute(sql);
		System.out.println("org.springframework.lengxf.tx.service.TxServiceC.test");
	}


}
